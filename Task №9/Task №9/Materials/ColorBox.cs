﻿namespace Task__9.Materials
{
    public class ColorBox : Material
    {
        public ColorBox(string color)
        {
            Color = color;
        }

        public string Color { get; }

        public override int Price => 40;
    }
}