﻿namespace Task__9.Materials
{
    public abstract class Material
    {
        public abstract int Price { get; }
    }
}