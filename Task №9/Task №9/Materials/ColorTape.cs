﻿namespace Task__9.Materials
{
    public class ColorTape : Material
    {
        public ColorTape(string color)
        {
            Color = color;
        }

        public string Color { get; }
        public override int Price => 15;
    }
}