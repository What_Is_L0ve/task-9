﻿using Task__9.Companies;

namespace Task__9.Parcels
{
    public class Package
    {
        public Package(string address, string addressee, CompanyOrder companyOrder, int price)
        {
            Address = address;
            Addressee = addressee;
            CompanyOrder = companyOrder;
            Price = price;
        }

        public string Address { get; }
        public string Addressee { get; }
        public CompanyOrder CompanyOrder { get; }
        public int Price { get; }
    }
}