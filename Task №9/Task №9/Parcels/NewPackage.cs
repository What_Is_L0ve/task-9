﻿using Task__9.Companies;

namespace Task__9.Parcels
{
    public class NewPackage : Package
    {
        public NewPackage(string address, string addressee, CompanyOrder companyOrder, int price)
            : base(address, addressee, companyOrder, price)
        {
        }
    }
}