﻿using Task__9.Companies;

namespace Task__9.Parcels
{
    public class StoragedPackage : Package
    {
        public StoragedPackage(string address, string addressee, CompanyOrder companyOrder, int price, int fullPrice)
            : base(address, addressee, companyOrder, price)
        {
            FullPrice = fullPrice;
        }

        public StoragedPackage(Package package, int fullPrice)
            : this(package.Address, package.Addressee, package.CompanyOrder, package.Price, fullPrice)
        {
        }

        public int FullPrice { get; }
    }
}