﻿using System.Collections.Generic;

namespace Task__9.Parcels
{
    public class PackageStorage
    {
        private Queue<StoragedPackage> _packages;

        public PackageStorage()
        {
            _packages = new Queue<StoragedPackage>();
        }

        public IReadOnlyCollection<StoragedPackage> Packages => _packages;

        public void Add(NewPackage newPackage, int fullPrice)
        {
            var storagedPackage = new StoragedPackage(newPackage, fullPrice);
            var list = new List<StoragedPackage>(_packages.ToArray());
            var changed = false;
            for (var i = 0; i < _packages.Count; i++)
                if (list[i].CompanyOrder.Priority < newPackage.CompanyOrder.Priority)
                {
                    list.Insert(i, storagedPackage);
                    changed = true;
                    break;
                }

            if (changed)
                _packages = new Queue<StoragedPackage>(list);
            else
                _packages.Enqueue(storagedPackage);
        }

        public IEnumerable<StoragedPackage> GetOrderForDelivery(int quality)
        {
            if (_packages.Count < quality) return _packages.ToArray();

            var result = new List<StoragedPackage>();
            for (var i = 0; i < quality; i++) result.Add(_packages.Dequeue());

            return result;
        }
    }
}