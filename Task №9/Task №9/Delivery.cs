﻿using Task__9.Calculators;
using Task__9.Parcels;

namespace Task__9
{
    public class Delivery
    {
        private readonly PackageStorage _packageStorage;
        private readonly PriceCalculator _priceCalculator;

        public Delivery(PriceCalculator priceCalculator, PackageStorage packageStorage)
        {
            _priceCalculator = priceCalculator;
            _packageStorage = packageStorage;
        }

        public void HandlePackage(NewPackage newPackage)
        {
            var fullPrice = _priceCalculator.CalculatorPrice(newPackage);
            _packageStorage.Add(newPackage, fullPrice);
        }
    }
}