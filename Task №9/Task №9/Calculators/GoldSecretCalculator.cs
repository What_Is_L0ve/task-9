﻿using Task__9.Companies;

namespace Task__9.Calculators
{
    public class GoldSecretCalculator : ICalculateStrategy
    {
        public int CalculatePrice(CompanyOrder companyOrder)
        {
            var result = companyOrder as GoldSecretCompanyOrder;
            if (result.Filler == null) 
                return companyOrder.Material.Price;
            return (companyOrder.Material.Price + result.Filler.Price) * 2;
        }

        public bool IsSuitable(CompanyOrder companyOrder)
        {
            return companyOrder is GoldSecretCompanyOrder;
        }
    }
}