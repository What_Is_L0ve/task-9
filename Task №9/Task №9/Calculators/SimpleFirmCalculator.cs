﻿using Task__9.Companies;

namespace Task__9.Calculators
{
    public class SimpleFirmCalculator : ICalculateStrategy
    {
        public int CalculatePrice(CompanyOrder companyOrder)
        {
            return companyOrder.Material.Price;
        }

        public bool IsSuitable(CompanyOrder companyOrder)
        {
            return companyOrder is SimpleFirmCompanyOrder;
        }
    }
}