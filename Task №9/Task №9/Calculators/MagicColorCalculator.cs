﻿using Task__9.Companies;

namespace Task__9.Calculators
{
    public class MagicColorCalculator : ICalculateStrategy
    {
        public int CalculatePrice(CompanyOrder companyOrder)
        {
            var result = companyOrder as MagicColorCompanyOrder;
            return companyOrder.Material.Price + result.ColorTape.Price;
        }

        public bool IsSuitable(CompanyOrder companyOrder)
        {
            return companyOrder is MagicColorCompanyOrder;
        }
    }
}