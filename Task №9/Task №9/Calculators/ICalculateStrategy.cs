﻿using Task__9.Companies;

namespace Task__9.Calculators
{
    public interface ICalculateStrategy
    {
        public bool IsSuitable(CompanyOrder companyOrder);
        public int CalculatePrice(CompanyOrder companyOrder);
    }
}