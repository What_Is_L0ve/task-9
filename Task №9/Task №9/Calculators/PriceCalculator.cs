﻿using System;
using Task__9.Parcels;

namespace Task__9.Calculators
{
    public class PriceCalculator
    {
        private readonly ICalculateStrategy[] _concreteCalculators;

        public PriceCalculator(ICalculateStrategy[] concreteCalculators)
        {
            _concreteCalculators = concreteCalculators;
        }

        public int CalculatorPrice(Package package)
        {
            foreach (var concreteCalculator in _concreteCalculators)
                if (concreteCalculator.IsSuitable(package.CompanyOrder))
                    return concreteCalculator.CalculatePrice(package.CompanyOrder) + package.Price;
            throw new ArgumentException("Неизвестная компания");
        }
    }
}