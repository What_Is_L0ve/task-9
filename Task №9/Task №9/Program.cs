﻿using System;
using Task__9.Calculators;
using Task__9.Companies;
using Task__9.Materials;
using Task__9.Parcels;

namespace Task__9
{
    public class Program
    {
        private static void Main(string[] args)
        {
            var priceCalculator = new PriceCalculator(new ICalculateStrategy[]
            {
                new GoldSecretCalculator(),
                new MagicColorCalculator(),
                new SimpleFirmCalculator()
            });

            var packageStorage = new PackageStorage();
            var delivery = new Delivery(priceCalculator, packageStorage);

            var packages = new[]
            {
                new NewPackage("Санкт-Петербург", "Синцев Владислав Олегович", new GoldSecretCompanyOrder(true), 400),
                new NewPackage("Санкт-Петербург", "Синцев Владислав Олегович", new MagicColorCompanyOrder(new ColorBox("Green"), new ColorTape("Black")), 200),
                new NewPackage("Москва", "Кузьмина Елена Владимировна", new SimpleFirmCompanyOrder(), 500),
                new NewPackage("Курган", "Афанасьев Сергей Викторович", new MagicColorCompanyOrder(new ColorBox("Blue"), new ColorTape("Red")), 1200),
                new NewPackage("Самара", "Непоспехова Диана Николаевна", new GoldSecretCompanyOrder(false), 650),
                new NewPackage("Камчатка", "Иванов Иван Иванович", new GoldSecretCompanyOrder(true), 5000)
            };

            foreach (var package in packages)
            {
                delivery.HandlePackage(package);
            }
            foreach (var package in packageStorage.Packages)
            {
                Console.WriteLine($"Был сделан заказ от {package.CompanyOrder.GetType().Name}");
                Console.WriteLine($"Город назначения: {package.Address}");
                Console.WriteLine($"Получатель: {package.Addressee}");
                Console.WriteLine($"Стоимость доставки: {package.Price}");
                Console.WriteLine($"Общая стоимость доставки: {package.FullPrice}\n");
            }

            Console.WriteLine("Доставка");
            foreach (var package in packageStorage.GetOrderForDelivery(4))
            {
                Console.WriteLine($"Был сделан заказ от {package.CompanyOrder.GetType().Name}");
                Console.WriteLine($"Город назначения: {package.Address}");
                Console.WriteLine($"Получатель: {package.Addressee}");
                Console.WriteLine($"Стоимость доставки: {package.Price}");
                Console.WriteLine($"Общая стоимость доставки: {package.FullPrice}\n");
            }
            Console.WriteLine("Склад");
            foreach (var package in packageStorage.Packages)
            {
                Console.WriteLine($"Был сделан заказ от {package.CompanyOrder.GetType().Name}");
                Console.WriteLine($"Город назначения: {package.Address}");
                Console.WriteLine($"Получатель: {package.Addressee}");
                Console.WriteLine($"Стоимость доставки: {package.Price}");
                Console.WriteLine($"Общая стоимость доставки: {package.FullPrice}\n");
            }
        }
    }
}