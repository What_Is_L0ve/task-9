﻿using Task__9.Materials;

namespace Task__9.Companies
{
    public class SimpleFirmCompanyOrder : CompanyOrder
    {
        public SimpleFirmCompanyOrder()
        {
            Material = new DefaultBox();
        }

        public override int Priority => 1;

        public override Material Material { get; }
    }
}