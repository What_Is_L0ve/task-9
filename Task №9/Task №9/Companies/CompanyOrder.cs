﻿using Task__9.Materials;

namespace Task__9.Companies
{
    public abstract class CompanyOrder
    {
        public abstract int Priority { get; }
        public abstract Material Material { get; }
    }
}