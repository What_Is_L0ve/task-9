﻿using Task__9.Materials;

namespace Task__9.Companies
{
    public class GoldSecretCompanyOrder : CompanyOrder
    {
        public GoldSecretCompanyOrder(bool isFragile)
        {
            Material = new TightBox();
            IsFragile = isFragile;
            if (isFragile) Filler = new Filler();
        }

        public override int Priority => 5;
        public bool IsFragile { get; }

        public override Material Material { get; }
        public Material Filler { get; set; }
    }
}