﻿using Task__9.Materials;

namespace Task__9.Companies
{
    public class MagicColorCompanyOrder : CompanyOrder
    {
        public MagicColorCompanyOrder(Material colorBox, Material colorTape)
        {
            Material = colorBox;
            ColorTape = colorTape;
        }

        public override int Priority => 3;

        public override Material Material { get; }

        public Material ColorTape { get; }
    }
}